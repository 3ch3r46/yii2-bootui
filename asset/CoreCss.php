<?php
namespace bootui\asset;
/**
 * Renderer bootstrap core css asset
 * @author Moh Khoirul Anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class CoreCss extends Asset
{
	public $css = [
		'css/bootstrap.css',
	];
}