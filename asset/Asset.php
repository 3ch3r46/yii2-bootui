<?php
namespace bootui\asset;

use yii\web\AssetBundle;
/**
 * Bootstrap Asset Bundle
 * @author Moh Khoirul Anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class Asset extends AssetBundle
{
	public $sourcePath = '@vendor/twbs/bootstrap/dist';
}