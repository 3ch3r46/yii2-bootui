<?php
namespace bootui\asset;
/**
 * Renderer core css bootstrap minified asset
 * @author Moh Khoirul Anam <3ch3r46@gmail.com>
 * @copyright 2014
 * @since 1
 */
class CoreCssMin extends Asset
{
	public $css = [
		'css/bootstrap.min.css',
	];
}